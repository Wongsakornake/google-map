/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import axios from 'axios';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAKSdlC37noLEChv31Oz64xpcrY-XPuV3Y',
        installComponents: true,
    }
})



const app = new Vue({
    el: '#app',
    data() {
        return {
            restaurants: [],
            area_position: [],
            search: '',
            map_center: { lat: 0, lng: 0 },
            infoWindowOptions: {
                pixelOffet: {
                    width: 0,
                    height: -35
                }
            },
            // infoWindowPosition: {
            //     lat: 0,
            //     lng: 0
            // },
            activeRestaurant: {},
            infoWindowOpened: false
        }
    },
    created() {
        axios.get('/api/restaurants')
            .then(response => {
                this.restaurants = response.data.restaurants;
                this.area_position = response.data.area;
                this.map_center = { lat: response.data.area.lat, lng: response.data.area.lng }
            });
    },
    methods: {
        getPosition(r) {
            return {
                lat: parseFloat(r.lat),
                lng: parseFloat(r.lng)
            }
        },
        searchPlace() {
            axios.get('/api/restaurants?search=' + this.search)
                .then(response => {
                    this.restaurants = response.data.restaurants;
                    this.map_center = { lat: response.data.area.lat, lng: response.data.area.lng }
                });
        },
        getPhotoSlideId(index) {
            return 'carouselSlide_' + index;
        },
        getLinkPhotoSlide(index) {
            return '#carouselSlide_' + index;
        },
        infoWindowPosition() {
            return {
                lat: parseFloat(this.activeRestaurant.lat),
                lng: parseFloat(this.activeRestaurant.lng)
            }
        },
        open_marker_info(r) {
            this.activeRestaurant = r;
            this.infoWindowOpened = true;
        },
        close_marker_info() {
            this.activeRestaurant = {};
            this.infoWindowOpened = false;
        },
        hide_marker(element_id, index_restaurant) {
            var ele = this.$refs[element_id];
            console.log(index_restaurant);
            console.log(element_id);
            console.log(ele);
            console.log(ele[0].checked);
            this.restaurants[index_restaurant].is_showing = ele[0].checked;
        }
    }
});
