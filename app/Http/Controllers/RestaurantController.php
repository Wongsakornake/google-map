<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RestaurantController extends Controller
{
    //
    public function index(Request $request)
    {
        $serach_keyword = (isset($request->search)) ? $request->search : 'บางซื่อ';
        $result = [];

        $result_api_area = Http::get("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?fields=formatted_address%2Cname%2Crating%2Copening_hours%2Cgeometry&input=$serach_keyword&inputtype=textquery&key=AIzaSyAKSdlC37noLEChv31Oz64xpcrY-XPuV3Y")->body();
        $result_api_area = json_decode($result_api_area, true);
        $location_lat = $result_api_area['candidates'][0]['geometry']['location']['lat'];
        $location_lng = $result_api_area['candidates'][0]['geometry']['location']['lng'];
        $result_api = Http::get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$location_lat%2C$location_lng&radius=1500&type=restaurant&key=AIzaSyAKSdlC37noLEChv31Oz64xpcrY-XPuV3Y")->body();
        $result_api = json_decode($result_api, true);

        foreach ($result_api["results"] as $k => $result_data) {
            $photos = [];
            if(isset($result_data["photos"])){
                foreach($result_data["photos"] as $n => $photo){
                    $photo_reference = $photo["photo_reference"];
                    $photo_url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=$photo_reference&key=AIzaSyAKSdlC37noLEChv31Oz64xpcrY-XPuV3Y";
                    $photos[] = $photo_url;
                }
            }

            $result['restaurants'][$k]["is_showing"] = true;
            $result['restaurants'][$k]["photo"] = $photos;
            $result['restaurants'][$k]["name"] = (isset($result_data["name"])) ? $result_data["name"] : "";
            $result['restaurants'][$k]["address"] = (isset($result_data["vicinity"])) ? $result_data["vicinity"] : "";
            $result['restaurants'][$k]["lat"] = (isset($result_data["geometry"])) ? $result_data["geometry"]["location"]["lat"] : "";
            $result['restaurants'][$k]["lng"] = (isset($result_data["geometry"])) ? $result_data["geometry"]["location"]["lng"] : "";
            $result['restaurants'][$k]["rating"] = (isset($result_data["rating"])) ? $result_data["rating"] : "0";
            $result['restaurants'][$k]["price_level"] = (isset($result_data["price_level"])) ? $result_data["price_level"] : "";
            $result['restaurants'][$k]["open_now"] = (isset($result_data["opening_hours"])) ? $result_data["opening_hours"]["open_now"] : "";
            $result['restaurants'][$k]["user_ratings_total"] = (isset($result_data["user_ratings_total"])) ? $result_data["user_ratings_total"] : "0";
        }
        $result['area']['lat'] = $location_lat;
        $result['area']['lng'] = $location_lng;

        return json_encode($result);
    }
}
